#include <stdio.h>
#include <stdlib.h>
#define MAX_CHUNK_COUNT  127

static size_t chunk_arr[MAX_CHUNK_COUNT] = {0};

void alloc() {
	int pos, size;
	void *chunk;

	printf("chunk position: ");
	scanf("%d", &pos);

	printf("chunk size : ");
	scanf("%d", &size);

	if(pos >= MAX_CHUNK_COUNT) {
		printf("Maximum number of chunks reached\n");
		exit(0);
	}

	if ((chunk = malloc(size)) == NULL) {
		printf("Out of memory\n");
		exit(0);
	}

	chunk_arr[pos] = (size_t)chunk;
}

void free_chunk() {
	int pos;

	printf("chunk position: ");
	scanf("%d", &pos);

	free((void*)chunk_arr[pos]);
}

void read_chunk() {
	int pos;
	size_t *values;

	printf("chunk position: ");
	scanf("%d", &pos);

	if(pos >= MAX_CHUNK_COUNT) {
		printf("Maximum number of chunks reached\n");
		exit(0);
	}

	values = (size_t*)chunk_arr[pos];
	printf("\nContents of chunk at %p: [%p]\n\n", values, (void*)*values);
}

int main(int argc, const char *argv[]){
	while(1) {	
		printf(" 1- Alloc\n 2- Free\n 3- Read\n 4- Exit\n\n");
		char ch = getchar();

		switch(ch) {
			case '1':
				alloc();
			break;

			case '2':
				free_chunk();
			break;

			case '3':
				read_chunk();
			break;

			case '4':
				exit(0);
			break;

			default:
				printf("Choose again\n\n");
		}
	}
	return 0;
}
