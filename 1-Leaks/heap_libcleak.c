
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>

int main() {
	void (*printf_addr)(char *) = dlsym(RTLD_NEXT, "printf");

	unsigned int *a = malloc(40),
		     *b = malloc(40);

	printf("\n-=-=-=-= Heap Memory Leak -=-=-=-=\n\n");
	printf("[+] a = malloc(40) @ %p\n", a);
	printf("[+] b = malloc(40) @ %p\n", b);

	printf("[*] Before free: b->fd == 0x%lx\n", *(size_t*)(b));

	printf("[+] free(a)\n");
	free(a);
	printf("[+] free(b)\n");
	free(b);
	
	unsigned int *d = malloc(40);	
	
	printf("[+] d = malloc(512) @ %p\n", d);
	printf("\n[*] Heap Leak\n");
	printf("[*] Use After free: d->fd == b->fd == 0x%lx\n\n", *(size_t*)(d));

	free(d);

	unsigned int *e = malloc(512),
		     *f = malloc(512),
		     *g = malloc(512);
	
	printf("-=-=-=-= Libc Memory Leak -=-=-=-=\n\n");
	printf("[+] e = malloc(512) @ %p\n", e);
	printf("[+] f = malloc(512) @ %p\n", f);
	printf("[+] g = malloc(512) @ %p\n", g);

	printf("[*] Before free: e->fd == 0x%lx\n", *(size_t*)(e));
	printf("[+] free(e)\n");
	free(e);

	printf("[+] free(g)\n");
	free(g);
	
	printf("[+] libc printf @ %p\n", (void*)printf_addr);
	unsigned int *h = malloc(512);	
	
	printf("[+] h = malloc(512) @ %p\n", h);
	
	printf("\n[*] Libc Leak\n");
	printf("[*] Use After free: h->fd == g->fd == 0x%lx\n", *(size_t*)(h));

	free(f);
	free(h);
	
	return 0;
}
