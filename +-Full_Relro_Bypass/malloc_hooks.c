#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <malloc.h>

static void *shell(size_t size, const void * caller) {
	system("sh");
}

int main() {
	__malloc_hook = shell;
	malloc(10);	
}
