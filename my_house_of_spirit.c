#include <stdio.h>
#include <stdlib.h>

// This technique is denoted as the house of spirit
// this technique resumes of making 2 fake stack fatsbins and get malloc to retrieve
// one of this chunks in future malloc calls. In order to achieve this, we have to
// some how store the address of one of this fake fastbins in a pointer and free this 
// pointer so that malloc returns a pointer to the region of our fake fastbin


int main(int argc, const char *argv[]){


	malloc(1);

	unsigned long long *a;
	unsigned long long fake_chunks[10];

	// this region countains two chunks, one starting at fake_chunks[1]
	// and another starting at fake_chunks[7]
	
	fake_chunks[1] = 0x41;
	// This is the initialization of the size field of the first fake chunk. 
	// the size is 0x30 + header size (0x10) + previous_inuse bit set
	
	fake_chunks[9] = 0x40;
	// this is the initialization of the size field of the second fake chunk.
	// this value must be small enough to be a fastbin. freebit doesnt matter
	
	// now we will overwrite our pointer with the address of the first fake-chunk
	// "chunk_data" (chunk+0x8) and then free it.
	
	a = &fake_chunks[2];
	free(a);

	// Now the next malloc of size 0x30 will return a pointer pointing to the region
	// of our first fake-chunk located in the stack
	unsigned long long *b = malloc(0x30);
	printf("%p\n", b);
		
	return 0;
}
